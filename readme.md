# KRAVSSPECIFIKATION

## INDHOLD

<details>

<summary>Se indhold</summary>

[TOC]

</details>

## ÆNDRINGSLOG

## 1. INDLEDNING

### 1.1 FORMÅL

Denne kravspecifikation definerer kravene til et program, som bruges til kundemodtagelse, håndtering af opgaver til mekanikere, og generering af fakturaer.

Systemet skal foretage følgende:

- Håndtere opgaver til mekanikere.
- Håndtere fakturaer.
- Generering af fakturaer baseret på færdige opgaver.

Systemet udvikles til FourWheels.

Systemet udvikles af firmaet SS.

Denne specifikation indgår som en del af aftalen mellem FourWheels, Kastanjerne og SS.

Ændringer til denne specifikation skal være godkendt af både FourWheels, Kastanjerne og SS.

### 1.2 REFERENCER

Følgende dokumenter er blevet brugt som referencemateriale for grundlaget af denne kravsspecifikation:
  * Interview med chef Jens Jensen fra Autoværkstedet ”FourWheel”
  * Kontrakt mellem FourWheels, Kastanjerne og SS
  * EUC Syd skabelon 2014

### 1.3 LÆSEVEJLEDNING
- Kapitel 2 indeholder de generelle krav, der gælder for systemet
- Kapitel 3 definerer de funktionelle krav ved hjælp af Use Case Teknikken
- Kapitel 4 beskriver produktets krav
- Kapitel 5 beskriver systemets IT krav

### 1.4 FORRETNINGSMÆSSIGE MÅL MED PROJEKTET

- Gøre det hurtigere og nemmere for kundemodtageren så kunden får en god oplevelse.
- Gøre det nemmere for mekanikerne at holde styr på brugte reservedele.
- Optimere arbejds hastighed i hele værkstedet.
- Automaticering af fakturaen.
- Skabe overblik over regnskaber.
- Gøre fremfinden af kunder og oprettelse af nye kunder hurtigere.

## 2. GENEREL BESKRIVELSE

### 2.1 SYSTEMBESKRIVELSE

#### 2.1.1 KONTEKST-DIAGRAM
Der ønskes udviklet et system til at gøre arbejdet nemmere for FourWheels' medarbejdere ved hjælp af et nyt IT System.

```plantuml
@startuml

' Configuration
'' Monocrhome and inverted colours
skinparam monochrome reverse
'' No shadows
skinparam shadowing false
'' Courier as the default font
skinparam defaultFontName Courier
'' Sequence messages are aligned according to direction
skinparam sequenceMessageAlign direction

' Diagram code

' Aktører
actor Kundemodtager
actor Mekaniker
actor Kunde
actor Chefens_Kone
actor Chef

' Systemer
[FourWheels]
[Økonomisystem]

' Flows
Kundemodtager --> [FourWheels]
Mekaniker --> [FourWheels]
Kunde --> [FourWheels]
Chef --> [FourWheels]
Chefens_Kone --> [FourWheels]

[FourWheels] <-> [Økonomisystem]

@enduml
```

Kundemodtageren, Mekanikerne, Kunden, Chefen og Konen benytter systemer, hvilket kommunikerer med økonomisystemet.

#### 2.1.2 AKTØR BESKRIVELSER

|  Aktør navn   |        Beskrivelse       |Antal|
|---------------|--------------------------|:-----:|
|Mekaniker      |Foretager Reparationer af biler |10|
|Reservedelsmand|Bestiller nye reservedele|1|
|Kundemodtager  |Modtager kunder og finder/opretter dem i systemet, opretter opgaver til mekanikerne|1|
|Chef           |Foretager salg af nye og brugte vogne|1|
|Chefens kone   |Foretager regnskab|1|

### 2.2 SYSTEMETS FUNKTIONER

De følgende diagrammer viser systemets funktioner udtrykt som Use Cases. Formålet med disse diagrammer er at give et overblik over funktionaliteten i det system, der skal udvikles. Den efterfølgende tekst beskriver hvordan de enkelte Use Cases spiller sammen.

```plantuml
@startuml
' Configuration
'' Monocrhome and inverted colours
skinparam monochrome reverse
'' No shadows
skinparam shadowing false
'' Courier as the default font
skinparam defaultFontName Courier
'' Sequence messages are aligned according to direction
skinparam sequenceMessageAlign direction

' Diagram code

' Aktører
actor Kunde
actor Receptionist
actor Mekaniker

' Værkstedssystemet
package "Subsystem: Værksted" {

  frame "Appointments" {
    [Opret appointment] --> [Læs appointment]
    [Læs appointment] --> [Redigér appointment]
    [Læs appointment] --> [Slet appointment]

  }

  frame "Opgaver" {
    [Læs opgave] --> [Opret opgave]
    [Læs opgave] --> [Slet opgave]
    [Læs opgave] --> [Redigér opgave]
  }

  frame "Kunder" {
    [Søg kunde] --> [Læs kunde]
    [Opret kunde] --> [Læs kunde]
    [Redigér kunde]
    [Slet kunde]
    [Læs kunde] --> [Redigér kunde]
    [Læs kunde] --> [Slet kunde]
    [Læs kunde] --> [Opret appointment]
  }

  frame "Fakturaer" {
    [Opret faktura]
    [Læs faktura] --> [Redigér faktura]
    [Læs faktura] --> [Send faktura]
  }

  ' Skal kunne læse kunde og deres appointments
  [Læs kunde] --> [Læs appointment]
  ' Oprettelse af appointments laver en første opgave som er opgave beskrivelsen
  [Opret appointment] --> [Opret opgave]
  ' Oprettelse af appointments lave en faktura i draft
  [Opret appointment] --> [Opret faktura]
  [Redigér appointment] --> [Læs opgave]
  ' Opgaver opdaterer fakturaen løbende
  Opgaver --> [Redigér faktura]

}

' Økonomisystemet - Anden leverandør
package "Subsystem: Økonomisystem" {
  [Modtag/fetch faktura]
}

' Kunde flow
Kunde --> [Læs kunde]
Kunde -down-> Receptionist

' Receptionist flow
Receptionist --> [Søg kunde]
Receptionist --> [Opret kunde]

' Mekaniker flow
Mekaniker -left-> [Læs appointment]

[Modtag/fetch faktura] <--> [Send faktura]
[Send faktura] --> Kunde
@enduml
```

Den typiske måde systemet andvendes på er følgende:

1. Kunden vender sig til systemet og booker en appointment.
    1. Alternativt kan kunden vende sig til receptionisten der derefter vil gøre det for kunden.
2. En appointment laves.
    1. Dette opretter en tom faktura.
    2. Dette opretter også en opgave.
        1. Denne opgave opfattes som opgavebeskrivelsen.
3. En mekaniker læser den første opgave fra denne appointment.
    1. Mekanikeren opretter opgaver baseret på opgavebeskrivelsen.
    2. Mekanikeren kan også ændre i opgavebeskrivelsen hvis mekanikeren ser det nødvendigt.
4. En eller flere mekanikere udarbejder opgaverne.
    1. Der kan oprettes flere opgaver imens arbejdet sker, hvis dette er nødvendigt og aftales med kunden.
    2. Opgaver kan redigeres undervejs.
    3. Opgaver kan slettes undervejs.
5. Når en opgave markeres færdig, sættes denne på fakturaen der fulgte med appointment.
6. Når appointment markeres færdig, markeres fakturaen som endelig.
7. Fakturaen bliver derefter sendt til økonomisystem og til kunden.

### 2.3 SYSTEMETS BEGRÆNSNINGER

- Skal overholde GDPR reglerne

### 2.4 SYSTEMETS FREMTID

- Mulighed for at sende fakturaer via mail

### 2.5 BRUGERPROFIL

Den daglige bruger er Kundemodtager og Mekanikerne. Kunden benytter også en hjemmeside for at se, ændre, og slette sine appointments.

Service på selve systemet laves a leverandøren. Brugerne (Kundemodtager, mekanikerne, samt deres kunder) skal have daglig adgang til systemet, dog begrænset i forhold til deres roller i virksomheden.

### 2.6 KRAV TIL UDVIKLINGSFORLØBET

1. SCRUM skal bruges.
2. Kodereviews skal forekomme.
3. Følgende programmeringssprog og værktøjer skal bruges.
    1. C#
        1. Entity Framework
        2. Razor Pages
    2. JS
    3. MariaDB
    4. Docker
    5. Kubernetes
    6. Git
4. Intern dokumentation skal laves med markdown.
5. Dokumentation der leveres til kunden skal leveres som PDF.
    1. Skal laves i en word processor. Eksempler:
        1. LibreOffice Writer
        2. OpenOffice Writer
        3. MS Word
        4. Google Docs
    2. En serif font skal bruges.
    3. Paragraffer skal have skriftstørrelsen 12.
    4. Hvis der forekommer kodesnippets, skal disse:
        1. Skrives med Courier fonten.
        2. Skriftstørrelsen 12.
        3. Eksistere i en grå boks.
6. Diagrammer skal kodes med [PlantUML](https://plantuml.com/]).
7. Der skal eksistere dokumentation af følgende:
    1. Intern dokumentation
        1. Selve programmet.
        2. Docker og Kubernetes opsætning.
        3. Vedligeholdelsesvejledning.
        4. Installationsvejledning.
        5. Servicevejledning.
    2. Kundedokumentation
        1. Bruger vejledning.
        2. Vejledning til værkstedets kunder (Booking af appointment).
        3. Leverandørkontakter alt efter ydelse.

### 2.7 OMFANG AF KUNDELEVERANCE

Første udgave af brgervejldeningen skal leveres til Kunden samt Kastanjerne, når kravspecifikationen er godkendt.

Efter at et stabilt system (version 1) er afleveret til Kunden, udleveres følgende dokumentation til Kunden i PDF form:

- Brugervejledning
- Servicedokumentation
- Installationsvejledning

Derudover skal Kunden også undervises i at bruge systemet i form af en workshop.

Efter hver iteration af et scrum, afleveres følgende opdateret dokumentation til Kastanjerne, hvis dokumentationen eksisterer:

- Brugervejledning
- Servicedokumentation
- Installationsvejledning
- Programdokumentation med kildetekster
- Designdokumentation
- Driftsdokumentation

### 2.8 FORUDSÆTNINGER

Der forudsættes, at nedenstående liste er til stede, og at interface og virkemåde af disse er som forventet og som vist i [2.2](#2-2-systemets-funktioner).

- Mekanikerne skal have en computer / tablet.
- Kundemodtageren skal have en computer / tablet.
- De ovenstående computere / tablets skal have en af følgende browsere installeret:
    - Chrome
    - FireFox
- Førnævnte hardware skal køre nyere versioner af operativsystemet, samt nyeste versioner af førnævnte browsere.
- Skal have en stabil og forholdsvis hurtig netværksforbindelse i form af WiFi og/eller RJ45 (ethernet).

## 3. DATAKRAV

### 3.1 DATAMODEL

![image](Billeder/FourWheelsER.drawio.png)

## 4. FUNKTIONSKRAV

### 4.1 PRODUKTKRAV

1. **Systemet skal kunne håndtere appointments.**
    1. Brugeren skal selv kunne booke, ændre og annullere sine appointments.
    2. Receptionisten skal kunne booke, ændre og annullere en kundes appointments.
2. **Systemet skal kunne håndtere opgaver.**
    1. Når en appointment laves, laver den en opgave - Denne betragtes som opgavebeskrivelsen.
    2. Mekanikerne skal kunne oprette, redigere og slette opgaver, samt markere dem som færdige.
3. **Systemet skal kunne håndtere fakturaer.**
    1. Når en appointment laves, laver den en faktura - Denne er kun en draft indtil alle opgaver i en appointment er markeret færdig.
    2. Når en opgave markeres som færdig, sættes denne på fakturaen automatisk.
    3. Det skal være muligt at redigere i fakturaen manuelt.
    4. Når alle opgaver er markeret som færdige i et appointment, markeres fakturaen som endelig, og sendes til økonomisystemet og kunden.
4. **Systemet skal kunne håndtere kunder.**
    1. Kunden skal kunne logge ind.
    2. Kunden skal kunne rette sine oplysninger.
    3. Kunden skal kunne slette sin bruger.
    4. Kunden skal kunne booke appointments.
    5. Receptionisten skal kunne lave 4.2 til 4.4 for en kunde.
    6. Receptionisten skal kunne oprette en kunde.
    7. Når kunden sletter sin brugerprofil, slettes fakturaer og igangværende appointments ikke.

### 4.2 USE CASES

Se use cases [her](Dokumenter/use-cases.pdf)

#### 4.2.1 Appointments

```plantuml
@startuml
' Configuration
'' Monocrhome and inverted colours
skinparam monochrome reverse
'' No shadows
skinparam shadowing false
'' Courier as the default font
skinparam defaultFontName Courier
'' Sequence messages are aligned according to direction
skinparam sequenceMessageAlign direction

(*) --> "Get customer information"

if "New customer" then
    --> "Create new customer"
    --> "Get customer"
else
    --> [else] "Get customer"
endif

--> [Appointments] ===B1===
--> "Create appointments"
--> ===B2===

===B1=== --> "Cancel Appointments"
--> ===B2===


===B1=== --> "Change Appointments"
--> ===B2===

--> (*)
@enduml
```

## 5. SYSTEMETS TEKNISKE OMGIVELSER/TEKNISK IT-ARKITEKTUR

### 5.1 NYT HARDWARE OG SOFTWARE
Som nævnt under punkt [2.8](#2-8-forudsætninger) bliver følgende udstyr krævet fremover:

- Mekanikerne skal have en computer / tablet.
- Kundemodtageren skal have en computer / tablet.
- De ovenstående computere / tablets skal have en af følgende browsere installeret:
    - Chrome
    - FireFox
- Førnævnte hardware skal køre nyere versioner af operativsystemet, samt nyeste versioner af førnævnte browsere.
- Skal have en stabil og forholdsvis hurtig netværksforbindelse i form af WiFi og/eller RJ45 (ethernet).

## 6. SIKKERHED

### 6.1 ADGANGSRET FOR BRUGERE

- Kundemodtager skal kunne logge ind og lave følgende:
    - Håndtere appointments for kunder.
    - Håndtere kunder.
- Mekanikere skal kunne logge ind og lave følgende:
    - Håndtere allerede eksisterende appointments.
    - Håndtere opgaver i appointments.
- Kunder skal kunne logge ind og lave følgende:
    - Rette sine oplysninger.
    - Slette sin bruger.
    - Oprette sine appointments.
    - Redigere sine appointments.
    - Annullere sine appointments.

### 6.2 SIKKERHEDSADMINISTRATION

- Det skal være muligt at administrere flere brugere; kunder så som ansatte.
- Det skal være muligt at fjerne tilladelse fra brugere; kunder så som ansatte.

### 6.3 SIKRING MOD TAB AF DATA

- Der skal tages backup af database hver anden time i form a SQL dumps.
- Fuld system backup skal forekomme engang i døgnet.
- SQL dumps skal gemmes i mindst 7 dage.
- Fuld system backups skal gemmes i mindst 31 dage.


### 6.4 SIKRING MOD UTILSIGTET BRUGERADFÆRD

- Der skal bruges captchas på brugernes formularer hvis de submitter for ofte (5 gange indenfor et minut)
- En konto låses hvis de skriver adgangskoden forkert 3 gange.

### 6.5 SIKRING MOD TRUSLER

- Der bruges certifikat til at kryptere kommunikationen mellem enhederne og systemet (HTTPS).
- Datatab - Læs 6.3.
- Bruteforcing - Læs 6.4.
- Systemet DDoS sikres så vidt muligt.
